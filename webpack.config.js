const path = require("path");

const globalConfig = {
    mode: 'none',
    optimization: {
        minimize: true,
    },
}

const sassFiles = [
    './src/scss/includes/_utilities.scss',
    './src/scss/includes/_global.scss',
    './src/scss/includes/_ally-toolbar.scss',
    './src/scss/includes/_header.scss',
    './src/scss/includes/_navigation.scss',
    './src/scss/includes/_main.scss',
    './src/scss/includes/_footer.scss',
    './src/scss/includes/_animations.scss',
    './src/scss/includes/_accessibility.scss',
    './src/scss/includes/_breakpoints.scss'
];

module.exports = [
    {
        ...globalConfig,
        name: "babelConfig",
        entry: [
            './src/js/index.js',
        ],
        output: {
            filename: "compiled-index.js",
            path: path.resolve(__dirname, "dist/js")
        },
        module: {
            rules: [
                {
                    // COMPILE ES6
                    test: /\.js$/i,
                    exclude: /(node_modules)/,
                    use: {
                        loader: "babel-loader",
                        options: {
                            presets: [
                                "@babel/preset-env"
                            ]
                        }
                    }
                }
            ]
        },
    },
    {
        ...globalConfig,
        name: "sassConfig",
        entry: [
            './src/scss/index.scss',
            ...sassFiles
        ],
        output: {
            filename: "compiled-css.js",
            path: path.resolve(__dirname, "dist/css")
        },
        module: {
            rules: [
                {
                    // COMPILE SASS/SCSS FILES AND PREFIX STYLES
                    test: /\.scss$/i,
                    exclude: /(node_modules)/,
                    use: [
                        {
                            loader: "style-loader" // FOR COMPILING SCSS
                        },
                        {
                            loader: 'file-loader', // FOR COMPILING SCSS TO SEPARATE CSS FILE
                            options: {
                                name: '[name].css'
                            }
                        },
                        {
                            loader: "postcss-loader", // FOR AUTO-PREFIXING STYLES
                            options: {
                                postcssOptions: {
                                    plugins: [
                                        [
                                            "autoprefixer",
                                            {
                                                // OPTIONS
                                            }
                                        ]
                                    ]
                                }
                            }
                        },
                        {
                            loader: "sass-loader", // FOR COMPILING SCSS
                            options: {
                                implementation: require("sass"),
                            }
                        },
                        {
                            loader: 'sass-resources-loader',
                            options: {
                                resources: [...sassFiles]
                            },
                        }
                    ]
                }
            ]
        }
    }
];