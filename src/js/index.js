const $ = require("jquery");

(function() {
    let Portfolio = {

        "NavBarIsSticky": false,
        
        "Init": function() {
            this.CheckHcMode();
            this.StickyNavBar();
            this.PositionSkipLink();
            this.MoveIntroImage();
            this.RevealEducation();
            this.RevealWorkSamples();
            this.Events();
            this.OpenInNewWindowWarning();

            $(window).on("scroll", () =>  { this.WindowScroll(); });
            $(window).on("resize", () =>  { this.WindowResize(); });
            $(window).on("load", () =>  { this.WindowLoad(); });
        },

        "WindowScroll": function() {
            this.StickyNavBar();
            this.MoveIntroImage();
            this.RevealEducation();
            this.RevealWorkSamples();
        },

        "WindowResize": function() {
            this.StickyNavBar();
            this.PositionSkipLink();
        },

        "WindowLoad": function() {
            this.OpenInNewWindowWarning();
        },

        "CheckHcMode": function() {
            if(this.ReadCookie("hc-mode")) {
                $("#ally-hc-toggle").attr("data-hc-state", "on");
                $("html").addClass("hc-mode");
            }
        },

        "StickyNavBar": function() {
            const scrollPosition = $(window).scrollTop();
            const navBarPosition = $("#nav-outer").offset().top;

            if(scrollPosition >= navBarPosition && !this.NavBarIsSticky) {
                $("#nav-outer").addClass("sticky").css("height", $("#nav").outerHeight(true));
                this.NavBarIsSticky = true;
            }
            if(scrollPosition < navBarPosition && this.NavBarIsSticky) {
                $("#nav-outer").removeClass("sticky").css("height", "");
                this.NavBarIsSticky = false;
            }
        },

        "MoveIntroImage": function() {
            const scrollPosition = $(window).scrollTop();
            const imagePosition = scrollPosition / 4;

            $("#intro-bg").css("top", imagePosition * -1);
        },

        "RevealEducation": function() {
            if(this.GetBreakPoint() == "x-large" && $("#education").hasClass("not-loaded")) {
                const scrollPosition = $(window).scrollTop() + $(window).height();
                const educationLoadPoint = $("#education").offset().top + $("#education").outerHeight(true) / 1.5;

                if(scrollPosition >= educationLoadPoint) {
                    $("#education .section-title").addClass("animate__animated animate__bounceInLeft").css("opacity", "1");
                    $("#education").removeClass("not-loaded");

                    $("#education ul li").each(function(i) {
                        setTimeout(() => {
                            $(this).addClass("animate__animated animate__bounceInLeft").css("opacity", "1");
                        }, (i + 1) * 100);
                    });
                }
            } else if(this.GetBreakPoint() != "x-large" && $("#education").hasClass("not-loaded")) {
                $("#education .section-title, #education ul li").css("opacity", "1");
                $("#education").removeClass("not-loaded");
            }
        },

        "RevealWorkSamples": function() {
            if(this.GetBreakPoint() == "x-large" && $("#work").hasClass("not-loaded")) {
                const scrollPosition = $(window).scrollTop() + $(window).height();
                const workLoadPoint = $("#work").offset().top + $("#work").outerHeight(true) / 1.5;

                if(scrollPosition >= workLoadPoint) {
                    $("#work-overview").addClass("animate__animated animate__bounceInLeft").css("opacity", "1");
                    $("#work").removeClass("not-loaded");

                    $("#work-samples ul li").each(function(i) {
                        setTimeout(() => {
                            $(this).addClass("animate__animated animate__bounceInUp").css("opacity", "1");
                        }, i * 100);
                    });
                }
            } else if(this.GetBreakPoint() != "x-large" && $("#work").hasClass("not-loaded")) {
                $("#work-overview, #work-samples ul li").css("opacity", "1");
                $("#work").removeClass("not-loaded");
            }
        },

        "PositionSkipLink": function() {
            const skipLinkHeight = $("#skip-link").outerHeight(true);

            $("#skip-link").css("margin-top", skipLinkHeight * -1);
        },

        "Events": function() {
            const _this = this;

            $(document).on("click keydown", "#ally-hc-toggle", function(e) {
                if(_this.AllyClick(e)) {
                    e.preventDefault();

                    const hcOff = $(this).attr("data-hc-state") == "off";

                    $(this).attr({
                        "data-hc-state": ((hcOff) ? "on" : "off"),
                        "aria-label": "Turn " + ((hcOff) ? "off" : "on") + " high contrast mode"
                    });
                    if(hcOff) {
                        $("html").addClass("hc-mode");
                        _this.CreateCookie("hc-mode", true);
                    } else {
                        $("html").removeClass("hc-mode");
                        _this.EraseCookie("hc-mode");
                    }

                    _this.PositionSkipLink();
                }
            });

            $(document).on("focus", "#skip-link a", function(e) {
                $(this).parent().animate({
                    "margin-top": "0px"
                }, 400);
            }).on("blur", "#skip-link a", function(e) {
                $(this).parent().animate({
                    "margin-top": $(this).parent().outerHeight(true) * -1
                }, 400);
            });

            $(document).on("click keydown", ".nav-link", function(e) {
                if(_this.AllyClick(e)) {
                    e.preventDefault();

                    $("html, body").animate({ scrollTop: $("#" + $(this).attr("data-section")).offset().top - $("#nav").outerHeight(true) }, "slow");
                }
            });

            $(document).on("click keydown", "#email", function(e) {
                if(_this.AllyClick(e)) {
                    e.preventDefault();

                    const characterMap = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","0","1","2","3","4","5","6","7","8","9","@","."];
                    const emailIndexes = [1,17,4,13,19,14,13,10,4,11,11,24,27,35,34,28,36,6,12,0,8,11,37,2,14,12];
                    let emailAddress = "";

                    $.each(emailIndexes, function(index, position) {
                        emailAddress += characterMap[position];
                    });
                    
                    window.location.href = "mailto:" + emailAddress;
                }
            });

            setInterval(function() {
                $("#scroll-down").removeClass("animate__animated animate__bounce animate__delay-2s");

                setTimeout(function() {
                    $("#scroll-down").addClass("animate__animated animate__bounce");
                }, 100);
            }, 5000);

            $(document).on("click keydown", "#scroll-down", function(e) {
                if(_this.AllyClick(e)) {
                    e.preventDefault();

                    $("html, body").animate({ scrollTop: $("#profile").offset().top - $("#nav").outerHeight(true) }, "slow");
                }
            });

            $(document).on("click keydown",  ".work-sample-button", function(e) { 
                if(_this.AllyClick(e)) {
                    e.preventDefault();
                    
                    $("body").css({
                        "overflow": "hidden",
                        "position": "relative"
                    });

                    $("#work ul li").removeClass("animate__animated animate__bounceInUp");

                    var workSampleContent = $("+ template", this).html();
                    $(this).parent().append(`
                        <div id="work-sample-detail-outer" role="dialog" aria-modal="true">
                            <div id="work-sample-detail" class="animate__animated animate__backInDown">
                                ${workSampleContent}
                                <a href="#" id="close-work-sample-modal" role="button" aria-label="Close ${$.trim($(this).text())} Project Detail Window"></a>
                            </div>
                        </div>
                    `);

                    setTimeout(() => {
                        $("#close-work-sample-modal").focus();
                    }, 1000);
                }
            }).on("focus", ".work-sample-button", function(e) {
                e.preventDefault();

                if($("#work").hasClass("not-loaded")) {
                    $("#work-overview").addClass("animate__animated animate__bounceInLeft").css("opacity", "1");
                    $("#work").removeClass("not-loaded");

                    $("#work-samples ul li").each(function(i) {
                        setTimeout(() => {
                            $(this).addClass("animate__animated animate__backInDown").css("opacity", "1");
                        }, i * 100);
                    });
                }
            });

            $(document).on("click keydown",  "#work-sample-detail-outer", function(e) { 
                if(_this.AllyClick(e)) {
                    e.preventDefault();

                    closeWorkSampleWindow();
                }
            });

            $(document).on("click keydown",  "#work-sample-detail", function(e) { 
                e.stopPropagation();

                if(e.type == "keydown" && e.code == "Escape") {
                    e.preventDefault();

                    closeWorkSampleWindow();
                }
            });

            $(document).on("click keydown", "#close-work-sample-modal", function(e) {
                if(e.type == "click") {
                    e.preventDefault();

                    closeWorkSampleWindow();
                }

                if(e.type == "keydown") {
                    if(e.code == "Space" || e.code == "Enter") {
                        e.preventDefault();

                        closeWorkSampleWindow();
                    }

                    if(e.code == "Tab" && !e.shiftKey) {
                        e.preventDefault();

                        $(".work-sample-wrapper a").first().focus();
                    }

                    if(e.code == "Tab" && e.shiftKey) {
                        e.preventDefault();
      
                        $(".work-sample-wrapper a").last().focus();
                    }
                }
            });

            $(document).on("keydown", ".work-sample-wrapper a:first", function(e) {
                if(e.code == "Tab" && e.shiftKey) {
                    e.preventDefault();

                    $("#close-work-sample-modal").focus();
                }
            });

            function closeWorkSampleWindow() {
                const workSample = $("#work-sample-detail-outer").parent().find(".work-sample-button");
                    
                $("#work-sample-detail").attr("class", "animate__animated animate__fadeOutDownBig");
                setTimeout(() => {
                    $("#work-sample-detail-outer").addClass("fade-out");

                    setTimeout(() => {
                        $("#work-sample-detail-outer").remove();
                        $("body").css("overflow", "");
                    }, 500);
                }, 200);
            }
        },

        "GetBreakPoint": function() {
            if(window.matchMedia("(max-width: 1399px)").matches) { return "large"; }

            else if(window.matchMedia("(max-width: 1199px)").matches) { return "medium"; }

            else if(window.matchMedia("(max-width: 999px)").matches) { return "small"; }

            else { return "x-large"; }
        },

        "AllyClick": function(event) {
            if(event.type == "click") {
                return true;
            } else if(event.type == "keydown" && (event.code == "Space" || event.code == "Enter")) {
                return true;
            } else {
                return false;
            }
        },

        "OpenInNewWindowWarning": function() {
        
            // INITIALLY ADD WARNING TO ALL LINKS WITH _blank TARGET VALUE
            $("a[target*='_blank']").each(function() {
                // IF THERE IS AN ARIA-LABEL PRESENT AND IT DOES NOT INCLUDE THE TEXT "Opens a new window"
                if($(this).attr("aria-label") && !$(this).attr("aria-label").includes("Opens a new window")) {
                    $(this).attr("aria-label", $(this).attr("aria-label") + " - Opens a new window");
                }
                
                // IF THERE IS AN ARIA-LABEL PRESENT AND THE LINK HAS NO TEXT BUT DOES HAVE AN IMAGE
                else if(!$(this).attr("aria-label") && $.trim($(this).text()) == "" && $("> img", this).length) {
                    $(this).attr("aria-label", $("> img", this).attr("alt") + " - Opens a new window");
                }
                
                // IF THERE IS NOT AN ARIA-LABEL PRESENT AND THE LINK TEXT DOES NOT INCLUDE "new" AND "window"
                else if(!$(this).attr("aria-label") && !$(this).text().toLowerCase().includes("new") && !$(this).text().toLowerCase().includes("window")) {
                    $(this).attr("aria-label", $(this).text() + " - Opens a new window");
                }
            });
    
            // ADD/REMOVE TAG
            $(document).off("touchend mouseenter mouseleave focus blur", "a[target*='_blank']").on("touchend mouseenter mouseleave focus blur", "a[target*='_blank']", function(e) {
                // IOS FREEZES NAVIGATION DUE TO THE WARNING TOOLTIP EVENT SO WE NEED TO FORCE NAVIGATION ON TOUCHEND
                var isIOS = /iPad|iPhone|iPod/.test(window.navigator.userAgent);
                if(isIOS) {
                    if(e.type == "touchend") {
                        window.open($(this).attr("href"), "_blank");
                        return false;
                    }
                } else {
                    if(e.type == "mouseenter" || e.type == "focusin") { // focusin BECAUSE JQUERY MAPS focus TO focusin
                        $("body").append('<div id="link-warning">Opens a new window</div>');
                    } else {
                        $("#link-warning").remove();
                    }
                }
            });
    
            // CREATE A DOM OBSERVER
            var observer = new MutationObserver(function(mutations) {
                // LOOP THROUGH LIST OF ELEMENTS THAT JUST CHANGED
                mutations.forEach(function(element) {
                    // TARGET ONLY <a> ELEMENTS
                    if(element.target.tagName == "A") {
                        // TARGET ONLY <a> ELEMENTS WITH A _blank TARGET VALUE
                        if($(element.target).attr("target") == "_blank") {
                            // ADJUST aria-label
                            if($(element.target).attr("aria-label") && !$(element.target).attr("aria-label").includes("Opens a new window")) {
                                $(element.target).attr("aria-label", $(element.target).attr("aria-label") + " - Opens a new window");
                            } else if(!$(element.target).attr("aria-label") && !$(element.target).text().toLowerCase().includes("new") && !$(element.target).text().toLowerCase().includes("window")) {
                                $(element.target).attr("aria-label", $(element.target).text() + " - Opens a new window");
                            }
                        }
                    }
                });    
            });
    
            // SET CONFIG OPTIONS FOR DOM OBSERVER
            var observerConfig = {
                attributes: true, 
                childList: true, 
                characterData: true,
                subtree: true
            };
    
            // SET NODE TO WATCH - WE'LL WATCH THE ENTIRE BODY ELEMENT AND ALL CHILDREN AS SET IN CONFIG
            var targetNode = document.body;
    
            // EXECUTE THE DOM OBSERVER
            observer.observe(targetNode, observerConfig);
            
        },

        "CreateCookie": function (name, value, days) {
            var expires;

            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = "; expires=" + date.toGMTString();
            } else {
                expires = "";
            }
            document.cookie = escape(name) + "=" + escape(value) + expires + "; path=/";
        },

        "ReadCookie": function(name) {
            var nameEQ = escape(name) + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) === ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) === 0) return unescape(c.substring(nameEQ.length, c.length));
            }
            return null;
        },

        "EraseCookie": function(name) {
            this.CreateCookie(name, "", -1);
        }

    };

    Portfolio.Init();
})();